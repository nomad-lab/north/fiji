# # recipe to create the nomad-remote-tools-hub apmtools container via a dockerfile
FROM gitlab-registry.mpcdf.mpg.de/nomad-lab/nomad-remote-tools-hub/webtop:v0.0.1

# USER root
ENV PATH=/usr/local/miniconda3/bin:$PATH
RUN mkdir -p /home \
  && mkdir -p /home/imagej/ImageJ/plugins/FAIRmat \
  && mkdir -p /home/fiji/Fiji.app

COPY Cheatsheet.ipynb FAIRmatNewLogo.png NOMADOasisLogo.png /home/
ADD FAIRmat /home/imagej/ImageJ/plugins/FAIRmat/
COPY 02-exec-cmd /config/custom-cont-init.d/02-exec-cmd

RUN apt update \
  && apt-get install -y git unzip wget libglu1-mesa-dev build-essential cmake \
  && wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh \
  && chmod +x Miniconda3-latest-Linux-x86_64.sh \
  && bash ./Miniconda3-latest-Linux-x86_64.sh -b -p /usr/local/miniconda3 \
  && rm -f Miniconda3-latest-Linux-x86_64.sh \
  && cd /home \
  && conda config --add channels conda-forge \
  && conda install python jupyter jupyterlab jupyterlab-h5web nodejs \
  && python -m pip install --upgrade pip \
  && conda clean -afy \
  && cd /home/fiji \
  && wget https://downloads.imagej.net/fiji/archive/20240614-2117/fiji-linux64.zip \
  && unzip fiji-linux64.zip \
  && rm -f fiji-linux64.zip \
  && cd /home/imagej \
  && wget https://wsr.imagej.net/distros/linux/ij154-linux64-java8.zip \
  && unzip ij154-linux64-java8.zip \
  && rm -f ij154-linux64-java8.zip \
  && chown -R ${PUID}:${PGID} /usr/local/miniconda3 \
  && chown -R ${PUID}:${PGID} /home

# customize the webtop autostart to spin up jupyter-lab
WORKDIR /home

#   && conda config --set channel_priority strict \
# get Michael Mohn et al. plugins for ImageJ (these have been developed for imagej)
# https://github.com/mmohn/SER_Reader.git (using e7828be)
# https://github.com/mmohn/Stack_Alignment.git (1563c59) from 1563c59 to 088ab1f only a README.md was added
# we are using here the files from the respective repository commits as indicated in the brackets
# the individual plug-in *.java source code files in this repository were then compiled using Compile and run
# on a Linux instance of the same imagej version as is unzip in this container
# the compiled plug-ins have been made available in /home/imagej/ImageJ/plugins/FAIRmat
# in ImageJ, we have done the compilation on the same ImageJ version which is used in this
# container, using the Compile and Run, the resulting java and class files are copied into the image
